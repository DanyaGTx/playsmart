const { src, dest, watch, series, parallel } = require('gulp');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync')
  .create();
const fileInclude = require('gulp-file-include');
function javascript(cb) {
  src('src/js/**/*.js')
    .pipe(dest('dist/js'))
  cb()
}
function scss(cb) {
  src([
    'src/scss/**/*pages/**/*.scss',
    'src/scss/**/*.scss',
    '!src/scss/**/*.module.scss',
  ])
    .pipe(sass()
      .on('error', sass.logError))
    .pipe(autoprefixer({
      cascade: false
    }))
    .pipe(dest('dist/css'))
    .pipe(browserSync.stream())

  cb()
}


function images(cb) {
  src('src/img/**/*')
    .pipe(dest('dist/img'))
  cb()
}
function sync(cb) {
  browserSync.init({
    server: {
      baseDir: 'dist',
      index: './index.html'
    },
    port: 3000,
  });
  if (cb) {
    cb()
  }
}
function browserReload(cb) {
  browserSync.reload()
  cb()
}
function processHTML(cb) {
  src('src/pages/**/*.html')
    .pipe(fileInclude({
      prefix: '@',
      basepath: '@file'
    }))
    .pipe(dest('dist'))

  cb()
}
function watchFiles() {
  watch('src/**/*.js', series(javascript, browserReload));
  watch('src/**/*.scss', scss);
  watch('src/**/*.html', series(processHTML, browserReload))
  watch('src/img/**/*', series(images, browserReload))
}

exports.default = parallel(series(processHTML, javascript, scss, images, sync), watchFiles)