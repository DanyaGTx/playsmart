# Setup project

> #### yarn install

> #### yarn start

# Project structure

`src/` - source code, dev folder

`src/components/` - components to including using `@include('.../components/name.html')` in `.html` files

`src/js/` - js files

`src/pages/` - actual html pages

`src/scss/` - scss files

# Mandatory information

- SCSS files that are not using in html must be named with `.module.scss` extension, for
  example `src/scss/variables.module.scss`
- In `src/pages/` html pages to import styles instead of `<link ... href="../scss/..."` **use
  ** `<link ... href="css/..."`
